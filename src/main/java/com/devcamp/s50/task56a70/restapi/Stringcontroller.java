package com.devcamp.s50.task56a70.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class Stringcontroller {
     @GetMapping("/length")
     public int getStringLength(@RequestParam("input") String input) {
          return input.length();
     }
}
